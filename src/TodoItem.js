/**
 * Individual todo items, layout
 */

import React from "react";

function TodoItem(props) {
    const todoChecked = {
        color: "#aaa",
        textDecoration: "line-through"
    }

    return (
        <div className="todo-item">
        <input
            type="checkbox"
            checked={props.item.checked}
            onChange={() => props.handleChange(props.item.id) }/>
        <p style={props.item.checked ? todoChecked : null}>{props.item.text}</p>
        <button className="todo-close">&#10005;</button>
        </div>
    )
}

export default TodoItem;