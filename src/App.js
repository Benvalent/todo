import React from 'react';
import './App.css';

import TodoItem from "./TodoItem";
import TodoItemAddNew from "./TodoItemAddNew";
import todoData from "./todoData";

class App extends React.Component {
  constructor() {
    super();
    this.state = {
      todosOld: todoData,
      newTodo: '',
      todos: [{
        id: 1,
        text: 'learn',
        checked: false
      }, {
        id: 2,
        text: "asdasd",
        checked: false
      }]
    }
    this.handleChange = this.handleChange.bind(this);
    //this.handleFormSubmitted = this.handleFormSubmitted.bind(this);
  }

  /* componentDidMount() */
  /* componentWillReceiveProps(nextProps) { if (nextProps.xy !== this.props.xy) { }} */
  /* getSnapshotBeforeUpdate() maybe for undo? */


  handleChange(id) {
    // Change 'checked' property
    this.setState(previousState => {
      const updateTodos = previousState.todosOld.map(todo => {
        if (todo.id === id) {
          todo.checked = !todo.checked;
        }
        return todo;
      })

      return {
        todosOld: updateTodos
      }
    })
  }

  newTodoChanged(event) {
    this.setState({
      newTodo: event.target.value
    })
  }

  handleFormSubmitted(event) {
    event.preventDefault();
    console.log(this.state.newTodo);
    this.setState({
      todos: [...this.state.todos, {
        text: this.state.newTodo,
        checked: false
      }]
    })
  }

  removeTodo(id) {

  }

  render() {
      const todoItems = this.state.todosOld.map(item => <TodoItem key={item.id} item={item} handleChange={this.handleChange} />)

      return (
        <div className="todo-list">
          <TodoItemAddNew />

          <form className="todo-item-add" onSubmit={(event) => this.handleFormSubmitted(event)}>
            <input
              type="text"
              id="todoNew"
              name="todoNew"
              placeholder="I have to..."
              onChange={(event) => this.newTodoChanged(event)} />
            <button>Add</button>
          </form>

          {todoItems}
        </div>
      );
  }

}

export default App;
